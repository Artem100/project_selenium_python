import os

from sqlalchemy import create_engine

db_ulr = ""
if "DB_URL" in os.environ:
    db_ulr = os.environ["DB_URL"]
else:
    db_ulr = str(os.getenv("DB_URL"))

db_name = ""
if "DB_NAME" in os.environ:
    db_name = os.environ["DB_NAME"]
else:
    db_name = str(os.getenv("DB_NAME"))

db_user_name = ""
if "DB_USER_NAME" in os.environ:
    db_user_name = os.environ["DB_USER_NAME"]
else:
    db_user_name = str(os.getenv("DB_USER_NAME"))


db_user_password = ""
if "DB_USER_PASSWORD" in os.environ:
    db_user_password = os.environ["DB_USER_PASSWORD"]
else:
    db_user_password = str(os.getenv("DB_USER_PASSWORD"))

# engine = create_engine("mysql+pymysql://ocuser:PASSWORD@192.168.0.100/opencart")
engine_conn="mysql+pymysql://{}:PASSWORD@{}/{}".format(db_user_name, db_ulr, db_name)
ENGINE = create_engine(engine_conn)