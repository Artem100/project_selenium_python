import os

from faker import Faker
from os.path import dirname, abspath
import logging

from src.sql.sql_engine import ENGINE

engine = ENGINE
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

customer_id_max_value = os.path.join(ROOT_DIR, "customer_id_max_value.sql")
create_new_user = os.path.join(ROOT_DIR, "create_new_user.sql")
select_create_customer = os.path.join(ROOT_DIR, "select_create_customer.sql")
# customer_id_max_value = ROOT_DIR + '\\customer_id_max_value.sql'

def create_new_user_sql():
    fake = Faker()
    firstname = fake.first_name()
    lastname = fake.last_name()
    email = "auto"+fake.email()
    password = '12345'
    password_text = '12345'

    try:
        logging.info("Max value of customer id")
        with open(customer_id_max_value, 'r') as script:
            sql_max_value_id = engine.execute(script.read())
        customer_id_value = sql_max_value_id.fetchall()[0][0]
    except Exception as e:
        print(e)
        assert False

    try:
        logging.info(f"Create user with params: customer_id: {customer_id_value}, firstname: {firstname}, lastname: {lastname}, email:{email}")
        with open(create_new_user, 'r') as script:
            engine.execute(script.read(), (customer_id_value, firstname, lastname, email, password))
    except Exception as e:
        print(e)
        assert False

    try:
        logging.info(f"Check new user in table with params: customer_id: {customer_id_value}, firstname: {firstname}, lastname: {lastname}, email:{email}")
        with open(select_create_customer, 'r') as script:
            created_user_prod = engine.execute(script.read(), (customer_id_value, firstname, lastname, email))
            assert len(created_user_prod.fetchall()) == 1
    except Exception as e:
        print(e)
        assert False

    return email, password_text