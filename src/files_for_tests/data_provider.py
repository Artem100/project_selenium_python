import os

import openpyxl

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
incorrect_username_and_passwords = customer_id_max_value = os.path.join(ROOT_DIR, "incorrect_username_and_passwords.xlsx")
# if platform == "linux" or platform == "linux2" or platform == "darwin":
#     file_path = ROOT_DIR + "/src/files_for_tests/"
# elif platform == "win32":
#     file_path = ROOT_DIR + "\\src\\files_for_tests\\"
#


def data_name_password():
    list = []
    wb = openpyxl.load_workbook(incorrect_username_and_passwords)
    print(wb.sheetnames) # Какие есть sheets

    sheet = wb['Login_passwords'] # Выбираем с какие sheet работать

    rows = sheet.max_row # Считываем максимальное количество строк
    cols = sheet.max_column # Считываем маскимальное количество колонок

    big_list = []
    for r in range(2, rows+1): # Чтобы не считывать название колонок задаем шаг 2
        username = sheet.cell(r, 1).value
        password = sheet.cell(r, 2).value

        tuple = (username, password)
        list.append(tuple)

    print(list)
    return list