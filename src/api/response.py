import logging


class ResponseChecking():

    def status_code_check(self, response, expected_code=200):
        logging.info(f"Check status is code: {expected_code}")
        if response.status_code == expected_code:
            pass
        else:
            logging.info(f"Incorrect status code. \nExpected value: 200,\n  Actual value: {response.status_code}")
            raise AssertionError(f"Incorrect status code. \nExpected value: 200,\n  Actual value: {response.status_code}")

    def extract_value(self, response, field_name):
        try:
            value = response.json()[field_name]
            return value
        except KeyError:
            assert Exception, "\nResponse body doesn't have *{}* field".format(field_name)

    def check_value_in_field(self, response, input_value, field_name):
        logging.info(f"Check value in key: {field_name}")
        try:
            value = response.json()[field_name]
            assert value == input_value
        except KeyError:
            logging.info("\nResponse body doesn't have *{}* field".format(field_name))
            assert Exception, "\nResponse body doesn't have *{}* field".format(field_name)
        except AssertionError:
            logging.info(f"\nKey [{field_name}] hasn't value: '{input_value}' \nIt's has value: '{value}'")
            assert False, f"\nKey [{field_name}] hasn't value: '{input_value}' \nIt's has value: '{value}'"

