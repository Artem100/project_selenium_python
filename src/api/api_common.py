import logging

import requests

from src.api.api_methods import AuthAsAdmin, AuthAsUser
from src.api.response import ResponseChecking


def authAdmin(username, password):
    data = {"username": username, "password": password}
    resp = AuthAsAdmin().authAdminApi(data)
    ResponseChecking().status_code_check(resp)
    cookies = resp.cookies.get_dict()
    return cookies["OCSESSID"]

def authUser(username, password):
    data = {"email": username, "password": password}
    resp = AuthAsUser().authUserApi(data)
    ResponseChecking().status_code_check(resp)
    cookies = resp.cookies.get_dict()
    return cookies["OCSESSID"]

