import logging
import os
import requests

from dotenv import load_dotenv

load_dotenv()
class ApiMethods():

    def __init__(self):
        if "URL_ENV" in os.environ:
            self._api_url = os.environ["URL_ENV"]
        else:
            self._api_url = str(os.getenv("URL_ENV"))

    def logger_for_api(self, response):
        logging.info("Request: \n url= {} \n header= {} \n body= {}".format(response.request.url, response.request.headers, response.request.body))
        logging.info("Response: \n status={} \n header={} \n body={}".format(response.status_code, response.headers,
                                                                            response.text))

    def _get_method(self, url, content_type='application/json', cookies="No"):
        logging.info("Method: GET")
        url_request = self._api_url+url
        headers = {'content-type': content_type}
        if cookies != "No":
            headers['Cookie'] = "language=en-gb; currency=USD; OCSESSID={}".format(cookies)
        response = requests.request("GET", url_request, headers=headers)
        self.logger_for_api(response)
        return response

    def _post_method(self, url, body, content_type='application/json', cookies="No"):
        logging.info("Method: POST")
        url_request = str(self._api_url + url)
        headers = {'content-type': content_type}
        if cookies != "No":
            headers['Cookie'] = "language=en-gb; currency=USD; OCSESSID={}".format(cookies)
        response = requests.request("POST", url_request, headers=headers, data=body)
        self.logger_for_api(response)
        return response

    def _put_method(self, url, body, content_type='application/json', cookies="No"):
        logging.info("Method: PUT")
        url_request = str(self._api_url + url)
        headers = {'content-type': content_type}
        if cookies != "No":
            headers['Cookie'] = "language=en-gb; currency=USD; OCSESSID={}".format(cookies)
        response = requests.request("POST", url_request, headers=headers, data=body)
        self.logger_for_api(response)
        return response

    def _delete_method(self, url, content_type='application/json', cookies="No"):
        logging.info("Method: DELETE")
        url_request = str(self._api_url + url)
        headers = {'content-type': content_type}
        if cookies != "No":
            headers['Cookie'] = "language=en-gb; currency=USD; OCSESSID={}".format(cookies)
        response = requests.request("POST", url_request, headers=headers)
        self.logger_for_api(response)
        return response


class AuthAsAdmin(ApiMethods):

    def authAdminApi(self, body):
        url = "/admin/index.php?route=common/login"
        response = self._post_method(url, body, content_type="multipart/form-data;")
        return response


class AuthAsUser(ApiMethods):

    def authUserApi(self, body):
        url = "/index.php?route=account/login"
        response = self._post_method(url, body, content_type="application/x-www-form-urlencoded")
        return response


class WishListApi(ApiMethods):

    def addToWishList(self, cookies, body):
        url = "/index.php?route=account/wishlist/add"
        response = self._post_method(url, body, cookies=cookies,
                                            content_type="application/x-www-form-urlencoded; charset=UTF-8")
        return response