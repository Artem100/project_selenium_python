import allure

from src.page_object.locators.shopping_cart_page import ShoppingCart


class ShoppingCartSteps(ShoppingCart):

    @allure.step("Check first row in shopping cart table")
    def shopping_cart_table_check_first_row(self, product_name, quantity, price):
        self.table_row_product_name(product_name)# "MacBook\nReward Points: 600")
        self.table_row_quantity_name_input_check_value(1, quantity)
        self.table_row_total_price_first_row(price) #"$500.00"