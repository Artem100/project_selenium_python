from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class ShoppingCart(BasePage):

    TABLE_ROWS_LIST = (By.CSS_SELECTOR, "div.table-responsive tbody>tr", "TABLE ROWS LIST")
    DELETE_PRODUCT_BUTTON = (By.CSS_SELECTOR, "div.table-responsive button.btn-danger", "DELETE PRODUCT BUTTON")

    def table_rows_list_count(self, count):
        self._count_of_elements(count, *self.TABLE_ROWS_LIST)
        return self

    def table_row_product_name(self, value):
        self._table_check_value(value, "PRODUCT NAME", 1, 2)
        return self

    def table_row_quantity_name_input_check_value(self, row_number, value):
        QUANTITY_TABLE = (By.CSS_SELECTOR, f"div.table-responsive tbody>tr:nth-child({row_number})>td:nth-child(4) input", "QUANTITY TABLE")
        self._check_attribute_value_in_element(value, *QUANTITY_TABLE)
        return self

    def table_row_total_price_first_row(self, value):
        self._table_check_value(value, "TOTAL PRICE TABLE", 1, 6)
        return self

    def delete_all_products_in_table(self):
        buttons = self._get_list_elements(*self.DELETE_PRODUCT_BUTTON)
        for button in buttons:
            button._click()