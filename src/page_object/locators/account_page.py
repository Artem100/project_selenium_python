import allure
from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class AccountPage(BasePage):

    ACCOUNT_PAGE_SUCCESS_URL = "/index.php?route=account/success"

    CONGRATULATIONS_TEXT_ELEMENT = (By.CSS_SELECTOR, "div#content>p:nth-child(2)", "CONGRATULATIONS TEXT ELEMENT")

    def at_account_page_url(self, env_url):
        self._check_url_of_page(env_url+self.ACCOUNT_PAGE_SUCCESS_URL)
        return self

    def congratulations_text_element_check_text(self):
        text = "Congratulations! Your new account has been successfully created!"
        self._check_text_in_element(text, *self.CONGRATULATIONS_TEXT_ELEMENT)
        return self

    @allure.step("Account page after success registration")
    def account_page_after_success_registration(self, env_url):
        self.congratulations_text_element_check_text()
        self.at_account_page_url(env_url)
        return self