import allure
from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class AdminPage(BasePage):

    TITLE_LOGIN = "Administration"
    TITLE_ADMIN_DASHBOARD = "Dashboard"
    TITLE_ADMIN_LOGIN = "Administration"

    USERNAME_FIELD = (By.CSS_SELECTOR, "input#input-username", "USERNAME FIELD")
    PASSWORD_FIELD = (By.CSS_SELECTOR,"input#input-password", "PASSWORD FIELD")
    SUBMIT_BUTTON = (By.CSS_SELECTOR, "button[type='submit']", "SUBMIT BUTTON")
    ERROR_MESSAGE = (By.CSS_SELECTOR, "div.alert-danger", "ERROR MESSAGE")

    @allure.step("Open admin page")
    def open_admin_page(self, env_url):
        self.open_page_via_url(env_url + "/admin")
        self._title_page_check(self.TITLE_LOGIN)
        return self

    @allure.step("Try to login in admin page")
    def login_to_admin_account(self, email, password):
        self._input_text(email, *self.USERNAME_FIELD)
        self._input_text(password, *self.PASSWORD_FIELD)
        self._click(*self.SUBMIT_BUTTON)
        return self

    @allure.step("User at login page")
    def at_admin_page(self):
        self._title_page_check(self.TITLE_ADMIN_DASHBOARD)
        return self

    @allure.step("User at login page")
    def at_login_admin_page(self):
        self._title_page_check(self.TITLE_ADMIN_LOGIN)
        return self

    @allure.step("Error message about incorrect creds")
    def incorrect_admin_creds(self):
        text = "No match for Username and/or Password.\n×"
        self._check_text_in_element(text, *self.ERROR_MESSAGE)
        return self