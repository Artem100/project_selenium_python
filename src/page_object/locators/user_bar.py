from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class UserBar(BasePage):

    SEARCH_FIELD = (By.CSS_SELECTOR, "#search input", "SEARCH FIELD")
    SEARCH_BUTTON = (By.CSS_SELECTOR, "#search button", "SEARCH BUTTON")
    SHOPPING_CART_BUTTON = (By.CSS_SELECTOR, "div#top-links a I.fa-shopping-cart", "SHOPPING CART BUTTON")
    SHOPPING_CART_BUTTON_DROPDOWN = (By.CSS_SELECTOR, "#cart button", "SHOPPING CART BUTTON DROPDOWN")
    CART_DROPDOWN_MENU_VALUE = (By.CSS_SELECTOR, "#cart ul.dropdown-menu p", "CART DROPDOWN MENU VALUE")
    MP3_PLAYER_DROPDOWN_LENGTH_ELEMENTS = (By.CSS_SELECTOR, "ul.nav.navbar-nav>li:last-child div.dropdown-inner>ul>li", "MP3 PLAYER DROPDOWN LENGTH ELEMENTS")
    MP3_PLAYER_DROPDOWN_BUTTON = (By.CSS_SELECTOR, "ul.nav.navbar-nav>li:last-child", "MP3 PLAYER DROPDOWN BUTTON")
    SUBMIT_BUTTON = (By.CSS_SELECTOR, "submit, input[type='submit']", "SUBMIT BUTTON")
    MAIN_LOGO_BUTTON = (By.CSS_SELECTOR, "div#logo a", "MAIN LOGO")
    ALERT_MESSAGE = (By.CSS_SELECTOR, "div.alert-success", "ALERT MESSAGE")
    WISH_LIST_BUTTON = (By.CSS_SELECTOR, "a#wishlist-total", "WISH LIST BUTTON")

    def search_button_click(self):
        self._click(*self.SEARCH_BUTTON)
        return self

    def search_field_input_value(self, value):
        self._input_text(value, *self.SEARCH_FIELD)
        return self

    def shopping_cart_click_dropdown(self):
        self._click(*self.SHOPPING_CART_BUTTON_DROPDOWN)
        return self

    def shopping_cart_dropdown_is_empty(self):
        text = "Your shopping cart is empty!"
        self._check_text_in_element(text, *self.CART_DROPDOWN_MENU_VALUE)
        return self

    def mp3_player_dropdown_check_default_count_elements(self):
        self._move_to_element(*self.MP3_PLAYER_DROPDOWN_BUTTON)
        self._count_of_elements_on_page(18, *self.MP3_PLAYER_DROPDOWN_LENGTH_ELEMENTS)
        return self

    def main_logo_button_click(self):
        self._click(*self.MAIN_LOGO_BUTTON)
        return self

    def shopping_cart_button_click(self):
        self._click(*self.SHOPPING_CART_BUTTON)
        return self

    def successfull_alert_shopping_cart(self, value):
        MESSAGE = f"Success: You have added {value} to your shopping cart!\n×"
        self._check_text_in_element(MESSAGE, *self.ALERT_MESSAGE)
        return self

    def successfull_alert_wish_list(self, value):
        MESSAGE = f"Success: You have added {value} to your wish list!\n×"
        self._check_text_in_element(MESSAGE, *self.ALERT_MESSAGE)
        return self

    def wish_list_button_click(self):
        self._click(*self.WISH_LIST_BUTTON)
        return self

    def table_element(self, row_number, column_number):
        PRODUCT_NAME = (By.CSS_SELECTOR, f"div.table-responsive tbody>tr:nth-child({row_number})>td:nth-child({column_number})", "PRODUCT NAME")
        return PRODUCT_NAME

    def submit_button_click(self):
        self._click(*self.SUBMIT_BUTTON)
        return self