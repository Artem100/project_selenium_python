from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class SearchPage(BasePage):

    EMPTY_SEARCH_RESULTS_ELEMENT = (By.CSS_SELECTOR, "#content > p:last-child", "EMPTY SEARCH RESULTS ELEMENT")
    FIRST_PRODUCT_IN_SEARCH_RESULT_BUTTON = (By.CSS_SELECTOR, "#content>div.row:nth-child(8)> .product-layout:first-child h4>a", "FIRST PRODUCT IN SEARCH RESULT BUTTON")

    def empty_search_results_check(self):
        empty_result_text = "There is no product that matches the search criteria."
        self._check_text_in_element(empty_result_text, *self.EMPTY_SEARCH_RESULTS_ELEMENT)
        return self

    def first_product_in_search_button_click(self):
        self._click(*self.FIRST_PRODUCT_IN_SEARCH_RESULT_BUTTON)
        return self