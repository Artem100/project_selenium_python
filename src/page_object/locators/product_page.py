from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class ProductPage(BasePage):

    PRODUCT_NAME = (By.CSS_SELECTOR, "#content>div>div:last-child h1", "PRODUCT NAME")
    ADD_TO_WISH_LIST_BUTTON = (By.XPATH, "//button[@data-original-title='Add to Wish List']", "ADD TO WISH LIST BUTTON")
    CART_BUTTON = (By.CSS_SELECTOR, "button#button-cart", "CART BUTTON")

    def product_name_check(self, value):
        self._check_text_in_element(value, *self.PRODUCT_NAME)
        return self

    def cart_button_click(self):
        self._click(*self.CART_BUTTON)
        return self

    def add_to_wish_list(self):
        self._click(*self.ADD_TO_WISH_LIST_BUTTON)
        return self