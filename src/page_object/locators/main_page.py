from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class MainPage(BasePage):

    FEATURED_CONTENT_FIRST_GOOD = (By.CSS_SELECTOR ,"div#content div.row>div.product-layout:first-child a", "FEATURED CONTENT FIRST GOOD")

    def open_main_page(self, url):
        title_page = "Your Store"
        self.open_page_via_url(url)
        self._title_page_check(title_page)
        return self

    def feature_content_fist_good_click(self):
        self._click(*self.FEATURED_CONTENT_FIRST_GOOD)
        return self