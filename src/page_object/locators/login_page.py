import allure
from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class LoginPage(BasePage):

    LOGIN_URL = "index.php?route=account/login"
    EDIT_ACCOUNT_BUTTON = (By.CSS_SELECTOR, "div#content a[href$='route=account/edit']", "EDIT ACCOUNT BUTTON")
    EMAIL_FIELD = (By.CSS_SELECTOR, "input#input-email", "EMAIL FIELD")
    PASSWORD_FIELD = (By.CSS_SELECTOR, "input#input-password", "PASSWORD FIELD")
    LOGIN_BUTTON = (By.CSS_SELECTOR, "input.btn-primary", "LOGIN BUTTON")

    def open_login_page_url(self, env_url):
        self.delete_cookies()
        self.open_page_via_url(env_url+self.LOGIN_URL)
        return self

    def login_valid_data(self, username, password):
        self._input_text(username, *self.EMAIL_FIELD)
        self._input_text(password, *self.PASSWORD_FIELD)
        self._click(*self.LOGIN_BUTTON)
        return self


    @allure.step("Login as user with cookies")
    def login_use_cookies(self, env_url, cookies):
        self.open_page_via_url(env_url)
        self.driver.delete_all_cookies()
        cookie =  {'name': "OCSESSID"}
        cookie["value"] = cookies
        self.driver.add_cookie(cookie)
        self.refresh_page()
        self._element_displayed(*self.EDIT_ACCOUNT_BUTTON)
        return self
