from src.page_object.base_page import BasePage


class WishListPage(BasePage):

    def table_first_row_product_name(self, value):
        self._table_check_value(value, "PRODUCT NAME", 1, 2)

    def table_first_row_model_name(self, value):
        self._table_check_value(value, "MODEL", 1, 3)

    def table_first_row_unit_price_name(self, value):
        self._table_check_value(value, "PRICE", 1, 5)
