import allure
from selenium.webdriver.common.by import By

from src.page_object.base_page import BasePage


class RegistrationPage(BasePage):

    REGISTRATION_URL = "/index.php?route=account/register"
    FIRST_NAME_FIELD = (By.CSS_SELECTOR, "input#input-firstname")
    LAST_NAME_FIELD = (By.CSS_SELECTOR, "input#input-lastname")
    EMAIL_FIELD = (By.CSS_SELECTOR, "input#input-email")
    TELEPHONE_FIELD = (By.CSS_SELECTOR, "input#input-telephone")
    PASSWORD_FIELD = (By.CSS_SELECTOR, "input#input-password")
    PASSWORD_CONFIRM_FIELD = (By.CSS_SELECTOR, "input#input-confirm")
    AGREE_CHECKBOX = (By.CSS_SELECTOR, "div.buttons input[name='agree']")
    CONTINUE_BUTTON = (By.CSS_SELECTOR, "input[type='submit']")

    def first_name_field_input(self, value):
        self._input_text(value, *self.FIRST_NAME_FIELD)
        return self

    def last_name_field_input(self, value):
        self._input_text(value, *self.LAST_NAME_FIELD)
        return self

    def telephone_field_input(self, value):
        self._input_text(value, *self.TELEPHONE_FIELD)
        return self

    def email_field_input(self, value):
        self._input_text(value, *self.EMAIL_FIELD)
        return self

    def password_field_input(self, value):
        self._input_text(value, *self.PASSWORD_FIELD)
        return self

    def password_confirm_field_input(self, value):
        self._input_text(value, *self.PASSWORD_CONFIRM_FIELD)
        return self

    def agree_checkbox_click(self):
        self._click(*self.AGREE_CHECKBOX)
        return self

    def continue_button_click(self):
        self._click(*self.CONTINUE_BUTTON)
        return self

    def open_registration_page_url(self, env_url):
        self.delete_cookies()
        self.open_page_via_url(env_url+self.REGISTRATION_URL)
        return self


