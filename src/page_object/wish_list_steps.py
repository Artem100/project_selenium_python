import allure

from src.page_object.locators.wish_list_page import WishListPage


class WishListSteps(WishListPage):

    @allure.step("Check value of product table")
    def table_fist_row_check_values(self, product_name, model_name, unit_price):
        self.table_first_row_product_name(product_name)
        self.table_first_row_model_name(model_name)
        self.table_first_row_unit_price_name(unit_price)