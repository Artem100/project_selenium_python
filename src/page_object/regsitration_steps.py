import allure

from src.page_object.locators.registration_page import RegistrationPage


class RegistrationSteps(RegistrationPage):

    @allure.step("Registration form - fill all fields")
    def registration_fill_all_fields(self, first_name, last_name, telephone, email, password, password_confirm):
        self.first_name_field_input(first_name)
        self.last_name_field_input(last_name)
        self.telephone_field_input(telephone)
        self.email_field_input(email)
        self.password_field_input(password)
        self.password_confirm_field_input(password_confirm)
        self.agree_checkbox_click()
