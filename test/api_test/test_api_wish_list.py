from src.api.api_methods import WishListApi
from src.api.json import JsonBody
from src.api.response import ResponseChecking

def test_0026(user_cookies):
    cookies = user_cookies
    resp = WishListApi().addToWishList(cookies, JsonBody().product_id_json("41"))
    ResponseChecking().status_code_check(resp)
    ResponseChecking().check_value_in_field(resp, "Wish List (1)", "total")
