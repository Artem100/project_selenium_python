import allure
import pytest

from src.page_object.locators.account_page import AccountPage
from src.page_object.locators.user_bar import UserBar
from src.page_object.regsitration_steps import RegistrationSteps

pytestmark = [pytest.mark.ui]

class TestRegistration:

    @pytest.fixture(autouse=True)
    def setup(self, browser, env_run, faker):
        self.first_name = faker.first_name()
        self.last_name = faker.last_name()
        self.telephone = faker.phone_number()
        self.email = "auto" + faker.email()
        self.password = faker.password(length=5)
        self.password_confirm = self.password

        RegistrationSteps(browser).open_registration_page_url(env_run)



    @allure.title("Positive registration")
    def test_08(self, browser, env_run):
        with allure.step("Fill all fields and click *Submit* button"):
            RegistrationSteps(browser).registration_fill_all_fields(self.first_name,
                                                                    self.last_name,
                                                                    self.telephone,
                                                                    self.email,
                                                                    self.password,
                                                                    self.password_confirm)
            UserBar(browser).submit_button_click()
        AccountPage(browser).account_page_after_success_registration(env_run)