import time

import pytest

from src.page_object.locators.login_page import LoginPage
from src.page_object.locators.main_page import MainPage
from src.page_object.locators.product_page import ProductPage
from src.page_object.locators.shopping_cart_page import ShoppingCart
from src.page_object.locators.user_bar import UserBar
from src.page_object.shoping_cart_steps import ShoppingCartSteps
from src.page_object.wish_list_steps import WishListSteps

pytestmark = [pytest.mark.ui]


class TestProductsPage():
    @pytest.fixture(autouse=True)
    def setup(self, browser, env_run, user_cookies):
        cookies = user_cookies
        main_browser = browser
        self.user_bar = UserBar(main_browser)
        self.login_page = LoginPage(main_browser)
        self.main_page = MainPage(main_browser)
        self.shopping_cart = ShoppingCart(main_browser)
        self.wish_list = WishListSteps(main_browser)
        self.product_page = ProductPage(main_browser)
        self.shopping_cart = ShoppingCartSteps(main_browser)

        self.main_page.open_main_page(env_run)
        # self.login_page.login_use_cookies(env_run, cookies)
        self.user_bar.main_logo_button_click()


    def test_06(self, browser):
        # self.user_bar.shopping_cart_button_click()
        # self.shopping_cart.delete_all_products_in_table()
        self.user_bar.main_logo_button_click()
        self.main_page.feature_content_fist_good_click()
        self.product_page.cart_button_click()
        self.user_bar.successfull_alert_shopping_cart("MacBook")
        self.user_bar.shopping_cart_button_click()
        self.shopping_cart.table_rows_list_count(1)
        time.sleep(5)
        self.shopping_cart.shopping_cart_table_check_first_row("MacBook\nReward Points: 600",
                                                                       "1",
                                                                       "$500.00")
    def test_07(self, browser):
        self.user_bar.main_logo_button_click()
        self.main_page.feature_content_fist_good_click()
        self.product_page.add_to_wish_list()
        self.user_bar.successfull_alert_wish_list("MacBook")
        self.user_bar.wish_list_button_click()
        self.wish_list.table_fist_row_check_values("MacBook",
                                                           "Product 16",
                                                           "$500.00")



