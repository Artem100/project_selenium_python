import pytest

from src.page_object.locators.login_page import LoginPage
from src.page_object.locators.main_page import MainPage
from src.page_object.locators.product_page import ProductPage
from src.page_object.locators.search_page import SearchPage
from src.page_object.locators.user_bar import UserBar

pytestmark = [pytest.mark.ui]


def test_02(browser, env_run):
    MainPage(browser).open_main_page(env_run)
    UserBar(browser).search_field_input_value("mp3")
    UserBar(browser).search_button_click()
    SearchPage(browser).empty_search_results_check()
#
def test_03(browser, env_run):
    MainPage(browser).open_main_page(env_run)
    UserBar(browser).search_field_input_value("iPod")
    UserBar(browser).search_button_click()
    SearchPage(browser).first_product_in_search_button_click()
    ProductPage(browser).product_name_check("iPod Classic1")

def test_04(browser, env_run):
    MainPage(browser).open_main_page(env_run)
    UserBar(browser).shopping_cart_click_dropdown().\
        shopping_cart_dropdown_is_empty()

def test_05(browser, env_run):
    MainPage(browser).open_main_page(env_run)
    UserBar(browser).mp3_player_dropdown_check_default_count_elements()

def test_100(browser, env_run, user_cookies):
    MainPage(browser).open_main_page(env_run)
    LoginPage(browser).login_use_cookies(user_cookies)
    UserBar(browser).refresh_page()
    pass

