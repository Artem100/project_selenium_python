import pytest

from src.files_for_tests.data_provider import data_name_password
from src.page_object.locators.admin_page import AdminPage

pytestmark = [pytest.mark.ui]


@pytest.mark.ui
def test_01(browser, env_run):
    AdminPage(browser).open_admin_page(env_run)
    AdminPage(browser).login_to_admin_account("admin", "admin")
    AdminPage(browser).at_admin_page()


@pytest.mark.parametrize("username, password", data_name_password())
def test_12(browser, env_run, username, password):
    AdminPage(browser).open_admin_page(env_run)
    AdminPage(browser).login_to_admin_account(username, password)
    AdminPage(browser).at_login_admin_page()
    AdminPage(browser).incorrect_admin_creds()