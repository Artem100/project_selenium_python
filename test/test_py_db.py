from faker import Faker
from sqlalchemy import create_engine

engine = create_engine("mysql+pymysql://ocuser:PASSWORD@10.0.6.74/opencart")

def test_make_prod():
    fake = Faker()

    # Creation prod
    LAST_PRODUCT_ID = engine.execute("select max(product_id)+1 from oc_product")
    NEW_PROD_ID = LAST_PRODUCT_ID.fetchall()[0][0]
    try:
        sql_insert_prod = "INSERT INTO oc_product(product_id) VALUES(%s)"
        engine.execute(sql_insert_prod, NEW_PROD_ID)
    except Exception as e:
        print(e)
    # Check created product
    sql = "SELECT * FROM opencart.oc_product where product_id = %s;"
    created_prod = engine.execute(sql, NEW_PROD_ID)
    assert len(created_prod.fetchall()) == 1


    # Update data to prod
    prod_name = fake.word()
    sql = "UPDATE oc_product SET model=%s WHERE product_id=%s"
    values_update = (prod_name, NEW_PROD_ID)
    engine.execute(sql, values_update)

    # Insert description to table for description for created prod
    description = fake.sentence()
    sql = "INSERT INTO oc_product_description(product_id, language_id, name, description, meta_keyword) VALUES (%s,%s,%s,%s,%s)"
    val = (NEW_PROD_ID, 1, prod_name, description, 'tag')
    engine.execute(sql, val)

    # Delete data from product
    sql = "DELETE FROM oc_product WHERE  product_id = %s"
    engine.execute(sql, NEW_PROD_ID)

    # Check that product was deleted
    sql = "SELECT * FROM oc_product where product_id = %s;"
    check_deleted_product = engine.execute(sql, NEW_PROD_ID)
    assert len(check_deleted_product.fetchall()) == 1

    # Delete data from product_description table
    sql = "DELETE FROM oc_product_description WHERE  product_id = %s"
    check_deleted_product = engine.execute(sql, NEW_PROD_ID)
    assert len(check_deleted_product.fetchall()) == 0

    # Check that product was deleted from product_description table
    sql = "SELECT * FROM oc_product_description where product_id = %s;"
    check_deleted_product_desc = engine.execute(sql, NEW_PROD_ID)
    assert len(check_deleted_product_desc.fetchall()) == 0

def test_create_user():
    fake = Faker()

